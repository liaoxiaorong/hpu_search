#coding=utf-8
import MySQLdb
import time
from sphinxapi import SphinxClient

class Mydb(object):
    @property
    def db(self):
        return MySQLdb.connect("localhost", "root", "asdfghjkl", "testx", charset='utf8')

    @classmethod
    def instance(cls):
        if not hasattr(cls, "_instance"):
            cls._instance = cls()
        return cls._instance    

    def _execute_with_commit(self, *args, **kwargs):
        conn = self.db
        cur = conn.cursor()
        cur.execute(*args, **kwargs)
        conn.commit()
        conn.close()                


class Pages(Mydb):
    def insert_data(self, d):
        self._execute_with_commit('insert ignore into pages (url_id, page_md5, md_time, title, ptext, filetype) values (%s, %s, %s, %s, %s, %s)',
                                  (d['url_id'], d['page_md5'], d['md_time'], d['title'], d['ptext'], d['filetype']))


class Msphinx(object):
    def __init__(self, host='localhost', port=9312):
        self.cl = SphinxClient()
        self.cl.SetServer(host, port)

    @classmethod
    def instance(cls):
        if not hasattr(cls, "_instance"):
            cls._instance = cls()
        return cls._instance    

    def query(self, q, offset=0, step=10, index='*'):
        self.cl.SetLimits(offset, step)
        return self.cl.Query(q, index)        

class Result(Mydb):
    def __init__(self, kw, pagenums=10):
        self.kw = kw
        self.pagenums = pagenums
        self._current_page = 1

    @property
    def qindex(self):
        return Msphinx.instance()

    def get_total(self):
        res = self.qindex.query(self.kw, 0, 1)
        total = res['total_found']
        timespent = res['time']
        return total, self._current_page, timespent

    def get_page(self, page):
        # page = int(page)
        self._current_page = page
        res = self.qindex.query(self.kw, (page-1)*10, self.pagenums)
        ids = [str(i['id']) for i in res['matches']]
        conn = self.db
        cur = conn.cursor()
        # cur.execute('select urls.title, urls.url, t.ptext, t.title '
        #             'from (select id, url_id, title, ptext from pages where id in (' +
        #             ",".join(["%s"] * len(ids)) + '))as t '
        #             'left join urls on t.url_id=urls.id', tuple(ids))
        rows = []
        for rid in ids:
            cur.execute('select urls.title, urls.url, t.ptext, t.title '
                        'from (select id, url_id, title, ptext from pages where id=%s)as t '
                        'left join urls on t.url_id=urls.id', (rid,))
            row = cur.fetchone()
            rows.append(row)
        conn.close()
        return rows


class Urls(Mydb):
    def __init__(self):
        self._current_id = 1

    def insert_urls(self, urls):
        conn = self.db
        cur = conn.cursor()
        for row in urls:
            cur.execute('insert urls (url, title) values (%s, %s) ON DUPLICATE KEY UPDATE pr=pr+1', (row[0], row[1]))
        conn.commit()
        conn.close()

    def _pop(self):
        conn = MySQLdb.connect("localhost", "root", "asdfghjkl", "testx", charset='utf8')
        cur = conn.cursor()
        cur.execute('select id from urls order by id desc limit 1')
        maxid = cur.fetchone()
        if not maxid or self._current_id > maxid[0]:
            raise Exception("No task!")
        cur.execute('select id, url, title from urls where id=%s', (self._current_id, ))
        row = cur.fetchone()
        self._current_id += 1
        return row

    def pop(self):
        while True:
            row = self._pop()
            if row:
                return row

if __name__ == "__main__":
    urls = Urls().instance()
    for i in range(3):
        print urls.pop()



