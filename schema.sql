SET SESSION storage_engine = "MyISAM"; 

ALTER DATABASE CHARACTER SET "utf8";

DROP TABLE IF EXISTS urls;
CREATE TABLE urls (
    id INT NOT NULL AUTO_INCREMENT,
    url varchar(255) NOT NULL,
    title varchar(255) NOT NULL,
    pr INT NOT NULL default 1,

    PRIMARY KEY (id),
    UNIQUE KEY `u_url` (`url`)
);

DROP TABLE IF EXISTS pages;
CREATE TABLE pages (
    id INT NOT NULL AUTO_INCREMENT,
    url_id int NOT NULL,
    md_time TIMESTAMP NOT NULL,
    page_md5 char(32) NOT NULL,
    title varchar(255) NOT NULL,
    ptext text NOT NULL,
    filetype varchar(30) NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY `u_pagemd5` (`page_md5`),
    FOREIGN KEY (url_id)
        REFERENCES urls(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

