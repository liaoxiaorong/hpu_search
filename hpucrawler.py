#coding=utf-8
import sys
import logging
import os

from webcrawler import WebPage
from hpudb import Pages, Urls
start_urls = [
    ('http://www.hpu.edu.cn/www/index.html', '河南理工大学'),
    ('http://tw.hpu.edu.cn/www/SiteFiles/Inner/dynamic/output.aspx?publishmentSystemID=1378&',''),
              ]              
logging.basicConfig(filename = os.path.join(os.getcwd(), './log/hpuerror.log'),
                    level = logging.INFO, filemode = 'w',
                    format = '%(asctime)s - %(levelname)s: %(message)s')
log = logging.getLogger()

def main():
    mdb = Pages().instance()
    urlset = Urls().instance()
    urlset.insert_urls(start_urls)
    while True:
        try:
            url_id, nurl, title = urlset.pop()
            print title, nurl
        except Exception,e:
            print e
            print 'crawled done'
            sys.exit(0)
        try:
            page = WebPage(nurl)
        except Exception,e:
            print e
            log.info(str(e)+nurl)
            continue
        pagedic = {
            'url_id': url_id,
            'page_md5': page.get_page_md5(),
            'md_time': page.last_modify_time(),
            'title': page.get_title(),
            'ptext': page.get_text().replace(' ', ''),
            'filetype': page.get_filetype(),
            }
        try:
            mdb.insert_data(pagedic)
        except Exception,e:
            print e
        urls = page.urls()
        urlset.insert_urls(urls)

if __name__ == "__main__":
    main()
