$(document).ready(function(){
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var kw = getParameterByName('kw');
    $("#resultlist").load("/q?kw="+encodeURIComponent(kw)+"&pn=");

    $('#resultlist').on('click', 'li', function(e) {
        var pn = this.id;;
        $("#resultlist").load("/q?kw="+encodeURIComponent(kw)+"&pn="+pn, function(){
            $(window).scrollTop(0);
            // $("html, body").animate({ scrollTop: 0 });
            // console.log("Now move");  
        });
    });

    $("#searchList button.close").click(function() {
        var that = $(this).parent();
        var id = $(this).data()["id"];
        $(that).load("/app/clearhistory/?id=" + id, function(){
            console.log("success");  
        });
        
    });
});