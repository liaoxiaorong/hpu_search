# coding: utf-8
import os
import urllib

import tornado
import tornado.ioloop
import tornado.httpserver
import tornado.web
from tornado.escape import json_encode

from hpudb import Result, Urls

from tornado.options import define, options
define("port", default=80, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self):
        base_dir = os.path.dirname(__file__)
        static_path = os.path.join(base_dir, "static")
        settings = {
            "cookie_secret": 'fjdkals;jfl;asjdfkl;jasd;jfl;dkalsjweurpq',
            "debug": True,
            }
        handlers = [
            (r"/?$", SearchHandler),
            (r"/q/?$", SearchAjaxHandler),
            (r"/about/?$", AboutHandler),
            (r"/test/?$", TestHandler),
            (r"/submiturl/?$", SubmitHandler),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': static_path}),
            ]
        tornado.web.Application.__init__(self, handlers, **settings)

class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        kw = self.get_argument("kw", default=None, strip=False)
        if kw:
            self.write(self.render_string('template/searchresult.html', title=kw))
        else:
            self.render("template/index.html")

class TestHandler(tornado.web.RequestHandler):
    def post(self):
        fileinfo = self.request.files['file']
        # fname = fileinfo['filename']
        self.write(str(fileinfo))
        return
        username = self.get_argument("username")
        passwd = self.get_argument("password")
        self.write(str(self.request.body))
        self.write(username+passwd)
    def get(self):
        self.write(str(self.request.body))

class SubmitHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('template/submiturl.html')
    def post(self):
        purl = self.get_argument("url", default=None, strip=False)
        ptitle = self.get_argument("title", default='', strip=False)        
        if purl:
            urlset = Urls().instance()
            urlset.insert_urls([(purl, ptitle)])
            msg = '提交网址成功, 点击回到<a href="/">首页</a>'
            self.render('template/message.html', msg=msg)
        else:
            self.render('template/submiturl.html')
            
class AboutHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('template/about.html')
        
class SearchAjaxHandler(tornado.web.RequestHandler):
    def get(self):
        kw = self.get_argument("kw", default='', strip=False)
        pn = self.get_argument("pn", default='1', strip=False)
        try:
            pn = int(pn)
        except:
            pn = 1
        if kw:
            pages = Result(kw)
            resultlist = []
            resultdic = {}

            rows = pages.get_page(pn)
            total, cur_page, timespent = pages.get_total()
            print "................", total
            print "kw:",kw
            if not int(total):
                self.write(u'<p id="noreuslt">搜索不到%s的相关信息</p>' % kw)
                return
            for n,  row in enumerate(rows, start=1):
                ptext = row[2]
                istart = ptext.find(kw)-10 if ptext.find(kw)-10 > 0 else 0
                iend = ptext.find(kw)+100
                title = row[3] if row[0] in row[3] else row[3]+' - '+row[0]
                resultdic = {
                    'title': title.replace(kw, '<em class="kw">'+kw+'</em>'),
                    'url': row[1],
                    'text': ptext[istart: iend].replace(kw, '<em class="kw">'+kw+'</em>'),
                    }
                resultlist.append(resultdic)
            pages = self.get_page_list(total, cur_page)
            self.render('template/resultlist.html', pages=pages, curpage=cur_page,
                                   resultlist=resultlist)
        else:
            self.redirect('/')

    def get_page_list(self, totalrecs, cur_page):
        totalpages = (totalrecs/10)+1 if totalrecs%10 else totalrecs/10
        pages = range(1, totalpages+1)
        istart = cur_page-4 if cur_page-4>0 else 0
        iend = cur_page+5
        return pages[istart:iend]
        


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port, address='0.0.0.0')
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()    
