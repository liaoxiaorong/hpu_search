# -*- coding: utf-8 -*- 

from sphinxapi import *
import sys, time

q = ''
# mode = SPH_MATCH_ALL
host = 'localhost'
port = 9312
index = '*'
filtercol = 'group_id'
filtervals = []
sortby = ''
groupby = ''
groupsort = '@group desc'
limit = 0

# do query
cl = SphinxClient()
cl.SetServer(host, port)
cl.SetLimits(0,10)
# cl.SetFieldWeights({'ptitle': 1, 'utitle': 1, 'ptext': 0.5})
# cl.SetWeights ([100, 0])
# cl.SetMatchMode(SPH_SORT_EXPR,'@weight+pr*')
# cl.SetMatchMode(SPH_SORT_EXPR,'@weight+pr*')
# cl.SetMatchMode(SPH_SORT_EXTENDED,'@weight, pr desc')
res = cl.Query('图书馆', index)
# print res
# for i in res['matches']:
#     print i, res['matches'][i]
# sys.exit()

if not res:
	print 'query failed: %s' % cl.GetLastError()
	sys.exit(1)

if cl.GetLastWarning():
	print 'WARNING: %s\n' % cl.GetLastWarning()

print 'Query \'%s\' retrieved %d of %d matches in %s sec' % (q, res['total'], res['total_found'], res['time'])
print 'Query stats:'

if res.has_key('words'):
	for info in res['words']:
		print '\t\'%s\' found %d times in %d documents' % (info['word'], info['hits'], info['docs'])

if res.has_key('matches'):
	n = 1
	print '\nMatches:'
	for match in res['matches']:
		attrsdump = ''
		for attr in res['attrs']:
			attrname = attr[0]
			attrtype = attr[1]
			value = match['attrs'][attrname]
			if attrtype==SPH_ATTR_TIMESTAMP:
				value = time.strftime ( '%Y-%m-%d %H:%M:%S', time.localtime(value) )
			attrsdump = '%s, %s=%s' % ( attrsdump, attrname, value )

		print '%d. doc_id=%s, weight=%d%s' % (n, match['id'], match['weight'], attrsdump)
		n += 1
