#coding=utf-8
import urllib2
import datetime
import re
import mimetypes
import urlparse
import hashlib

import nltk
import xlrd

class WebPage(object):
    """ A web page class """
    re_charset = re.compile('content="text/html; charset=(.*?)"', re.IGNORECASE)
    re_link = re.compile('<a.*?href=[\'"]?(.*?)[\'"].*?>(.*?)</a')
    re_ip = re.compile('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
    re_title = re.compile('<title>(.*?)</', re.IGNORECASE)
    # re_words = re.compile("\W+")

    def __init__(self, url):
        self.url = url
        req = urllib2.Request(url)
        req.add_header('User-Agent','Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)')
        req = urllib2.urlopen(req, timeout=20)
        self._md_time = req.info().getheader('last-modified')
        self.filetype = req.info().getheader('Content-Type')
        # charset = req.headers.getparam('charset')
        if self.filetype == 'text/html':
            html = req.read()
            charset = self.re_charset.findall(html)
            if charset:
                html = html.decode(charset[0], 'ignore').encode('utf-8')
            else:
                html = html.decode('utf-8', 'ignore').encode('utf-8')
            self.html = html
        else:
            self.html = self._get_content_baseon_filetype(req, self.filetype)

    def _get_content_baseon_filetype(self, req, filetype):
        if filetype == 'application/msword':
            contentstr = ' '
        elif filetype == 'application/vnd.ms-excel':
            contentstr = ''
            xls = xlrd.open_workbook(file_contents=req.read())
            sheets = xls.sheets()
            for sheet in sheets:
                nrows = sheet.nrows
                for i in range(1, nrows):
                    row = sheet.row_values(i)
                    rowstr = '\t'.join(row[1:]).encode('utf-8')
                    contentstr += rowstr+'\n'
                contentstr += '\n'
        else:
            contentstr = ' '
        return contentstr

    def last_modify_time(self):
        if self._md_time:
            try:
                return datetime.datetime.strptime(self._md_time, '%a, %d %b %Y %H:%M:%S %Z')
            except:
                return ''
        else:
            return ''

    def get_page_md5(self):
        return hashlib.md5(self.html).hexdigest()

    def get_text(self):
        # return nltk.clean_html(self.html) #.replace('\r\n', '\t').replace('\t', '')
        text =  nltk.clean_html(self.html) #.replace('\r\n', '\t').replace('\t', '')
        entries = re.split("\r\n+", text)
        entries = [line.strip().rstrip() for line in entries if line.strip().rstrip()]
        return '\n'.join(entries)

    def get_filetype(self):
        return self.filetype

    def urls(self):
        url = urlparse.urlparse(self.url)
        links = self.re_link.findall(self.html)
        urls = {}
        for (link, title) in links:
            link = link.decode("utf-8", "replace")
            title = title.decode("utf-8", "replace")
            if link.startswith('/'):
                link = 'http://' + url[1] + link
            elif link.startswith('#'):
                continue
                # link = 'http://' + url[1] + url[2] + link
            elif not link.startswith('http'):
                link = 'http://' + url[1] + '/' + link
            if 'hpu.edu.cn' in link or self.re_ip.findall(link):
                tags = re.findall('(<.*?>)', title)
                if tags:
                    for tag in tags:
                        title = title.replace(tag, '')
                urls[link] = title
        return [(k, urls[k]) for k in urls]

    def get_title(self):
        # print self.html
        title = self.re_title.findall(self.html)
        return title[0] if title else ''

if __name__ == "__main__":
    # page = WebPage('http://wfstc.hpu.edu.cn/UploadFiles/200941517295736.xls')
    page = WebPage('http://lib.hpu.edu.cn/count/tj.asp?id=16')
    # page = WebPage('http://www.hpu.edu.cn/www/index.html')
    print page.last_modify_time()
    print page.get_filetype()
    print page.get_text()
    print page.get_title()
    # print page.get_text()
    # print page.get_page_md5()
    # page.urls()
    urls = page.urls()
    for i, url in enumerate(urls, start=1):
        print i, url[0].encode('utf-8'), url[1].encode('utf-8')
        # print i, '\t'.join(url)
