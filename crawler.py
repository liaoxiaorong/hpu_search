#coding=utf-8
import sys
import re
import urllib2
import urlparse
tocrawl = set([sys.argv[1]])
crawled = set([])
titlere=re.compile('<title>(.*?)</title>')
linkregex = re.compile('<a.*?href=[\'|"](.*?)[\'|"]')
charsetre=re.compile('charset="?(.*?)"')
jpgre=re.compile('(.*?).jpg$')

Start_Url = ''

while 1:
    try:
        crawling = tocrawl.pop()
        if len(jpgre.findall(crawling)):
            continue
        print crawling
    except KeyError:
        raise StopIteration
    url = urlparse.urlparse(crawling)
    try:
        response = urllib2.urlopen(crawling)
    except:
        continue
    msg = response.read()
    links = linkregex.findall(msg)
    crawled.add(crawling)
    for link in (links.pop(0) for _ in xrange(len(links))):
        if link.startswith('/'):
            link = 'http://' + url[1] + link
        elif link.startswith('#'):
            link = 'http://' + url[1] + url[2] + link
        elif not link.startswith('http'):
            link = 'http://' + url[1] + '/' + link
            if link not in crawled:
                tocrawl.add(link)
                charset=charsetre.findall(msg)
                if len(charset):
                    char=charset[0]
                else:
                    char="utf-8"
    titles=titlere.findall(msg)
    if len(titles):
        title=titles[0]#.decode(char,'ignore')
        print title+"\n"
                                                                                                                                    

def re_find_item(reg_text, msg):
    items = re.findall(reg_text.decode('UTF-8').encode('UTF-8'),msg, re.DOTALL)
    return items[0] if items else ''
    # return items if items else ''

def re_find_lyurls(reg_text, msg):
    items = re.findall(reg_text.decode('UTF-8').encode('UTF-8'),msg, re.DOTALL)
    return items if items else []
    # return items if items else ''    

if __name__ == "__main__":
    print 'start...'
